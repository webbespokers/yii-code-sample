<?php

/**
 * This is the model class for table "journal_events".
 *
 * The followings are the available columns in table 'journal_events':
 * @property integer $id
 * @property integer $user_id
 * @property string $activity
 * @property string $date_start
 * @property string $date_end
 * @property string $location
 * @property string $conditions
 * @property string $objective
 * @property double $percentage_programm
 * @property double $percentage_system
 * @property string $what_did
 * @property string $what_did_well
 * @property string $solution
 * @property string $statement
 */
class JournalEvents extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'journal_events';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'user_id, activity, date_start, date_end, location, conditions, objective',
                'required'
            ),
            array('user_id', 'numerical', 'integerOnly' => true),
            array('percentage_programm, percentage_system', 'numerical'),
            array('location, conditions', 'length', 'max' => 11),
            array(
                'what_did, what_did_well, solution, statement',
                'filter',
                'filter' => array(new CHtmlPurifier(), 'purify')
            ),
            array(
                'id, user_id, activity, date_start, date_end, location, conditions, objective, percentage_programm, percentage_system, what_did, what_did_well, solution, statement',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user'   => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'activity' => 'Activity',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'location' => 'Location',
            'conditions' => 'Conditions',
            'objective' => 'Objective',
            'percentage_programm' => 'Percentage Programm',
            'percentage_system' => 'Percentage System',
            'what_did' => 'What Did',
            'what_did_well' => 'What Did Well',
            'solution' => 'Solution',
            'statement' => 'Statement',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('activity', $this->activity, true);
        $criteria->compare('date_start', $this->date_start, true);
        $criteria->compare('date_end', $this->date_end, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('conditions', $this->conditions, true);
        $criteria->compare('objective', $this->objective, true);
        $criteria->compare('percentage_programm', $this->percentage_programm);
        $criteria->compare('percentage_system', $this->percentage_system);
        $criteria->compare('what_did', $this->what_did, true);
        $criteria->compare('what_did_well', $this->what_did_well, true);
        $criteria->compare('solution', $this->solution, true);
        $criteria->compare('statement', $this->statement, true);

        return new CActiveDataProvider(
            $this,
            array(
                'criteria'=>$criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return JournalEvents the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (is_array($this->activity)) {

            $this->activity = implode(',', $this->activity);
        }


        return parent::beforeSave();
    }

    public function afterSave()
    {
        if (! is_array($this->activity)) {

            $this->activity = explode(',', $this->activity);
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if (! is_array($this->activity)) {

            $this->activity = explode(',', $this->activity);
        }

        return parent::afterFind();
    }

    public function beforeValidate()
    {
        $this->user_id = Yii::app()->user->id;

        return parent::beforeValidate();
    }

    /**
     * if $userId isn't set, function gets events for current authorized user
     *
     * @param null $userId user id
     * @return array of user journal events
     */
    public function getEventsForUser($userId = null)
    {
        if ($userId == null) {

            $userId = Yii::app()->user->id;
        }

        $events = Yii::app()->db->createCommand()
            ->select('e.*')
            ->from('journal_events AS e')
            ->where('e.user_id = :USER_ID AND date_start >= ' . date('Y-m-d', strtotime('-1 year')),
                array(
                    ':USER_ID' => $userId,
                )
            )
            ->queryAll();

        $result = array();

        foreach ($events as $event) {

            $result[] = array(
                'id'    => $event['id'],
                'title' => $event['objective'],
                'start' => $event['date_start'],
                'end'   => $event['date_end'],
            );
        }

        return $result;
    }
}
