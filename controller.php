<?php

class CalendarController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'manageEvent', 'viewEvent', 'deleteEvent'),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     * @return Courses
     */
    public function loadModel($id)
    {
        $model = JournalEvents::model()->findByPk($id);

        if ($model === null) {

            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    public function actionIndex($userId = null)
    {
        if (!Yii::app()->user->isAdmin() && !Yii::app()->user->model()->hasAccessToCalendarOfCourse) {

            throw new CHttpException(404);
        }

        if ($userId != null) {

            $user = User::model()->findByPk($userId);

            if (empty($user)) {

                throw new CHttpException(404);
            }
        }

        $events = JournalEvents::model()->getEventsForUser($userId);

        for ($i = 0 ; $i < count($events); $i++) {

            if (strlen($events[$i]['title']) > 30) {

                $events[$i]['title'] = substr($events[$i]['title'], 0, 30) . '...';
            }
        }

        $model = new JournalEvents();

        $model->date_start = date('Y-m-d');
        $model->date_end = date('Y-m-d');
        $model->percentage_programm = 0;
        $model->percentage_system = 0;

        $this->render(
            'index',
            array(
                'events' => $events,
                'model' => $model
            )
        );
    }

    /**
     * Manage creating and updating event model
     *
     * @throws CHttpException 404
     */
    public function actionManageEvent()
    {
        $journalEventsRequest = Yii::app()->request->getParam('JournalEvents', false);

        $badRequest = !Yii::app()->request->isAjaxRequest || !$journalEventsRequest;

        if ($badRequest) {

            throw new CHttpException(404);
        }

        $model = new JournalEvents();

        $success = false;

        $newEvent = isset($journalEventsRequest['id']) && empty($journalEventsRequest['id']);

        if (!$newEvent) {

            $model = $this->loadModel($journalEventsRequest['id']);
        }

        $model->attributes = $journalEventsRequest;

        if ($model->validate()) {

            $model->save();

            $success = true;
        }

        $html = $this->renderPartial('_create_form', array('model' => $model), true);

        $result = array(
            'html'    => $html,
            'success' => $success
        );

        Yii::app()->end(CJSON::encode($result));
    }

    public function actionViewEvent()
    {
        if (!Yii::app()->request->isAjaxRequest) {

            throw new CHttpException(404);
        }

        $id = Yii::app()->request->getParam('id', null);

        if (!$id) {

            throw new CHttpException(404);
        }

        $model = $this->loadModel($id);

        if ($model->user_id != Yii::app()->user->id) {

            throw new CHttpException(404);
        }

        $html = $this->renderPartial('_create_form', array('model' => $model), true);

        $result = array(
            'html' => $html
        );

        Yii::app()->end(CJSON::encode($result));
    }

    public function actionDeleteEvent()
    {
        if (!Yii::app()->request->isAjaxRequest) {

            throw new CHttpException(404);
        }

        $id = Yii::app()->request->getParam('id', null);

        if (!$id) {

            throw new CHttpException(404);
        }

        $model = $this->loadModel($id);

        if($model->delete()) {

            Yii::app()->end(CJSON::encode('ok'));
        }
    }
}
