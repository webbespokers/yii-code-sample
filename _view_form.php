<?php

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    array(
        'id' => 'event-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('calendar/createEvent'),
        'htmlOptions' => array('class' => 'form-horizontal')
    )
);

echo $form->hiddenField($model, 'id');

echo $form->errorSummary($model);

?>

<div class="control-group <?php if ($model->hasErrors('activity')) { echo 'error'; } ?>">
    <label class="control-label">
        Activity For the Day <span class="required">*</span>
    </label>
    <div class="controls">
    <?php

    echo $form->checkBoxList(
        $model,
        'activity',
        array(
            'Trading' => 'Trading',
            'Study/Review' => 'Study/Review',
            'Simulation' => 'Simulation'
        ),
        array(
            'template' => '<label class="checkbox inline">{input}{label}</label>',
            'uncheckValue' => null
        )
    );

    ?>
    </div>
</div>

<div class="control-group" id="event-date">
    <label class="control-label">Date</label>
    <div class="controls">
        <span class="text-label" id="date-interval">
        <?php

        echo date('d/m/Y', strtotime($model->date_start))
            . ' - '
            . date('d/m/Y', strtotime($model->date_end));
        ?>
        </span>
        <?php

        echo $form->hiddenField($model, 'date_start', array('id' => 'event-date-start'));

        echo $form->hiddenField($model, 'date_end', array('id' => 'event-date-end'));

        ?>
    </div>
</div>

<div class="control-group <?php if ($model->hasErrors('location')) { echo 'error'; } ?>">
    <label class="control-label">
        Location <span class="required">*</span>
    </label>
    <div class="controls">
        <?php

        echo $form->radioButtonList(
            $model,
            'location',
            array(
                'Home office' => 'Home office',
                'Laptop' => 'Laptop',
                'Traveling' => 'Traveling'
            ),
            array(
                'template' => '<label class="radio inline">{input}{label}</label>',
                'uncheckValue' => null
            )
        );

        ?>
    </div>
</div>

<div class="control-group <?php if ($model->hasErrors('conditions')) { echo 'error'; } ?>">
    <label class="control-label">
        Conditions <span class="required">*</span>
    </label>
    <div class="controls">
    <?php

    echo $form->radioButtonList(
        $model,
        'conditions',
        array(
            'Trading Day' => 'Trading Day',
            'Holiday' => 'Holiday',
            'Weekend' => 'Weekend'
        ),
        array(
            'template' => '<label class="radio inline">{input}{label}</label>',
            'uncheckValue' => null
        )
    );

    ?>
    </div>
</div>

<div class="control-group <?php if ($model->hasErrors('objective')) { echo 'error'; } ?>">
    <label class="control-label">Objective <span class="required">*</span></label>
    <div class="controls">
    <?php

    echo $form->textArea($model, 'objective', array('class' => 'input-block-level', 'rows'=>'4'));

    ?>
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('percentage_programm')) { echo 'error'; } ?>">
    <label class="control-label">% Ran My Mind Program For the Day</label>
    <div class="controls">
     <?php

    echo $form->textField($model, 'percentage_programm', array('style' => 'width:30px', 'maxlength' => '3', 'size' => '3'));

    ?> %
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('percentage')) { echo 'error'; } ?>">
    <label class="control-label">% Ran My Technical Systems Correctly</label>
    <div class="controls">
    <?php

    echo $form->textField($model, 'percentage_system', array('style' => 'width:30px', 'maxlength' => '3', 'size' => '3'));

    ?> %
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('what_did')) { echo 'error'; } ?>">
    <label class="control-label">What I Did Today & What I Learned</label>
    <div class="controls">
    <?php

    echo $form->textArea($model, 'what_did', array('class' => 'input-block-level', 'rows'=>'4'));

    ?>
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('what_did_well')) { echo 'error'; } ?>">
    <label class="control-label">What I Did Well Today</label>
    <div class="controls">
    <?php

    echo $form->textArea($model, 'what_did_well', array('class' => 'input-block-level', 'rows'=>'4'));

    ?>
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('solution')) { echo 'error'; } ?>">
    <label class="control-label">I Am Looking For A Solution To</label>
    <div class="controls">
    <?php

    echo $form->textArea($model, 'solution', array('class' => 'input-block-level', 'rows'=>'4'));

    ?>
    </div>
</div>
<div class="control-group <?php if ($model->hasErrors('statement')) { echo 'error'; } ?>">
    <label class="control-label">Directive Affirmation Statement</label>
    <div class="controls">
    <?php

    echo $form->textArea($model, 'statement', array('class' => 'input-block-level', 'rows'=>'4'));

    ?>
    </div>
</div>

<?php $this->endWidget(); ?>

<div class="modal-footer">
    <a href="#" class="btn btn-primary" id="submitEventForm">
        <?php echo $model->isNewRecord ? 'Create' : 'Update'; ?>
    </a>
    <?php

    if (!$model->isNewRecord) {

        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Remove',
                'type' => 'danger',
                'size' => 'small',
                'htmlOptions' => array('id' => 'removeEvent'),
            )
        );
    }

    ?>
</div>